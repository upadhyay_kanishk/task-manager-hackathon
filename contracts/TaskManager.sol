// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;
pragma experimental ABIEncoderV2;

import { Task, User, TaskManagerLibrary }  from "./TaskManagerLibrary.sol";

// Creating a contract
contract TaskManager {
    using TaskManagerLibrary for User[];
    using TaskManagerLibrary for string;

    // Defining a variable to store the task ids which
    // will be assigned to every task which is created
    uint256 private taskIdCounter;

    mapping(uint256 => Task[]) private TaskList;

    // Defining function to increment taskIdCounter
    // before creating a new task.
    function increaseTaskIdCounter() internal {
        taskIdCounter++;
    }

    // Defining function to create a task
    function createTask(
        uint256 _userId,
        string calldata _task,
        string calldata _date
    ) external {
        require (!_task.stringsEquals(""), "Task Name required");
        increaseTaskIdCounter();

        TaskList[_userId].push(
            Task({
                id: taskIdCounter,
                task: _task,
                isCompleted: false,
                date: _date
            })
        );
    }

    // Defining a function to update status of a task
    function toggleStatus(uint256 _userId, uint256 _taskIndex) external {
        TaskList[_userId][_taskIndex].isCompleted = !TaskList[_userId][
            _taskIndex
        ].isCompleted;
    }

    // Defining a function to delete a task
    function deleteTask(uint256 _userId, uint256 _taskIndex) external {
        if (_taskIndex >= TaskList[_userId].length) return;

        for (uint i = _taskIndex; i<TaskList[_userId].length-1; i++){
            TaskList[_userId][i] = TaskList[_userId][i+1];
        }
        TaskList[_userId].pop();
    }

    // Defining a function to get user's task count.
    function getTaskCount(uint256 _userId) external view returns (uint256) {
        return TaskList[_userId].length;
    }

    // Defining a function to get user's tasks.
    function getTaskList(uint256 _userId)
        external
        view
        returns (Task[] memory)
    {
        return TaskList[_userId];
    }

    // Now functions for UserLogin
    
    // Defining a variable to store the user ids which
    // will be assigned to every user which is created
    uint256 private userIdCounter;

    User[] private UserList;

    // Defining function to increment userIdCounter
    // before creating a new User.
    function increaseUserIdCounter() internal {
        userIdCounter++;
    }

    // Defining function to create a User
    function createUser(
        string calldata _userLoginId,
        string calldata _userName,
        string calldata _userPasswordHash
    ) external {
        require (!_userLoginId.stringsEquals(""), "LoginId required");
        require (!UserList.userExists(_userLoginId), "User already exists");
        increaseUserIdCounter();

        UserList.push(
            User({
                id: userIdCounter,
                loginId: _userLoginId,
                name: _userName,
                password: _userPasswordHash
            })
        );
    }

    // Defining a function to get user's List.
    function getUserList()
        external
        view
        returns (User[] memory)
    {
        return UserList;
    }

    // Defining a function to login
    function login(
        string calldata _userLoginId,
        string calldata _userPasswordHash
    ) external view returns (uint256)
    {
        bool isLoginSuccess = false;
        uint256 userId = 0;
        for (uint i = 0; i<UserList.length; i++){
            if(UserList[i].loginId.stringsEquals(_userLoginId) 
                && UserList[i].password.stringsEquals(_userPasswordHash)) {
                    isLoginSuccess = true;
                    userId = UserList[i].id;
            }
        }

        require(isLoginSuccess && userId != 0, "Invalid UserName or Password");
        return userId;
    }
}
