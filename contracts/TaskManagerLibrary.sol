// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

// Defining a structure to
// store a task (Data Structure for our task)
struct Task {
    uint256 id;
    string task;
    bool isCompleted;
    string date;
}

// Defining a structure to
// store a User Detail (Data Structure for our user)
struct User {
    uint256 id;
    string loginId;
    string name;
    string password;
}

library TaskManagerLibrary {

    // Function check if userLoginId exists
    function userExists(
      User[] calldata userList, string calldata loginId) public pure returns (
      bool) {
        bool isUserExists = false;
        for (uint i = 0; i < userList.length; i++){
            if(stringsEquals(userList[i].loginId, loginId)){
                isUserExists = true;
            }
        }
        return isUserExists;
    }
    
    function stringsEquals(
        string memory s1,
        string memory s2
    ) public pure returns (bool) {
        bytes memory b1 = bytes(s1);
        bytes memory b2 = bytes(s2);
        uint256 l1 = b1.length;
        if (l1 != b2.length) return false;
        for (uint256 i=0; i<l1; i++) {
            if (b1[i] != b2[i]) return false;
        }
        return true;
    }
}