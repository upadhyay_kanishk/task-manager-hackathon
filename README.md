# Task Manager Hackathon

## Getting started

### Steps to Deploy Smart Contracts

- Install ganache and truffle first to get started. Ganache will be your local Ethereum wallet and truffle will be used to compile and deploy smart contracts.
  ```
  npm i -g ganache
  npm i -g truffle
  ```
- Now run ganache to use ethereum wallet on your machine. Use the below command to run ganache. This will run ganache and keep this terminal open.
  ```
  ganache
  ```
- Now we need truffle to compile the smart contracts so that they can be deployed. Run below command in a new terminal window.
  ```
  truffle compile
  ```
- Now we need to deploy the compiled smart contracts.
  ```
  truffle migrate
  ```

Now smart contact is deployed and can be used. To test smart contract in terminal use truffle console. To connect to truffle console use the below command.
```
truffle console
```

To get smart contract address open a `truffle console` and use below commands.
```
app = await TaskManager.deployed()
app.address
```

## Troubleshooting

- If there is problem connection to ganache please check if `truffle-config.js` has same correct ganache address and port.