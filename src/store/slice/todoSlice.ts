import { createSlice } from '@reduxjs/toolkit';

const todo = createSlice({
      name: 'todo',
      initialState: {
            todo: [],
      },
      reducers: {},
});

export const {} = todo.actions;

export default todo.reducer;
