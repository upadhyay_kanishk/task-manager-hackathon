export interface FormList {
    placeholder?: string | undefined;
    icon?: React.ReactNode;
    type?: string;
    value?: string;
}