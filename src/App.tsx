import {
  BrowserRouter as Router,
  Routes,
  Route,
  BrowserRouter,
} from "react-router-dom";
import SignUp from "./components/SignUp";
import SignIn from "./components/SignIn";
import Table from "./components/Table";
import { Provider } from "react-redux";
import store from "./store/store";
import Web3 from "web3";
import { useEffect, useState } from "react";
import { AbiItem } from "web3-utils";
import { ABI, ContractAddress } from "./contract-abi";

const App = () => {
  const ethereum = (window as any).ethereum;
  const web3 = new Web3(ethereum);
  const [tasksList, setTasksList] = useState([]);
  const [numberOfTasks, setNumberOfTasks] = useState(0)
  async function getAccount() {
    const accounts = await ethereum.request({
      method: "eth_requestAccounts",
    });

    web3.eth.defaultAccount = accounts[0];
    // console.log(web3.eth.defaultAccount + " account detected");
    // console.log(web3);
    const contract = new web3.eth.Contract(ABI as AbiItem[], ContractAddress, {
      from: web3.eth.defaultAccount || "",
    });
    try {
      contract.methods
        .getTaskList(1)
        .call()
        .then((x: any) => {
          console.log(x);
          setTasksList(x);
        });
    } catch (ex) {
      console.log(ex);
    }

    try {
      contract.methods
        .getTaskCount(1)
        .call()
        .then((x: any) => {
          setNumberOfTasks(x)
        });
    } catch (ex) {
      console.log(ex);
    }
    return web3.eth.defaultAccount;
  }
  useEffect(() => {
    getAccount();
  }, []);

  function handleUpdateTask(task: any) {}
  function handleDeleteTask(task: any) {}

  return (
    <BrowserRouter>
      <Provider store={store}>
        <Routes>
          <Route path="/signup" element={<SignUp />}></Route>
        </Routes>
        <Routes>
          <Route path="/login" element={<SignIn />}></Route>
        </Routes>
        <Routes>
          <Route
            path="task/:id"
            element={
              <Table
                totalTask={numberOfTasks}
                tableDataSource={tasksList}
                onUpdateTask={(task) => handleUpdateTask(task)}
                onDeleteTask={(task) => handleDeleteTask(task)}
              />
            }
          ></Route>
        </Routes>
      </Provider>
    </BrowserRouter>
  );
};

export default App;
