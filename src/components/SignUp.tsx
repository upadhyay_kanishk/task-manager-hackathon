import Card from "./Card";
import LoginImage from "../images/signup-image.jpg";
import FormField from "./FormField";
import {
  FilePersonFill,
  Mailbox2,
  Unlock,
  UnlockFill,
} from "react-bootstrap-icons";
import { FormList } from "../models/form-interface";
import { Link } from "react-router-dom";
import { useState } from "react";

const SignUp = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const formList: FormList[] = [
    {
      placeholder: "Enter your name",
      icon: <FilePersonFill />,
      value: name,
    },
    {
      placeholder: "Enter your email",
      type: "email",
      icon: <Mailbox2 />,
      value: email,
    },
    {
      placeholder: "Enter password",
      type: "password",
      icon: <UnlockFill />,
      value: password,
    },
    {
      placeholder: "Confirm password",
      type: "password",
      icon: <Unlock />,
      value: confirmPassword,
    },
  ];

  const handleInputChange = (event: any, placeholder: string | undefined) => {
    console.log(placeholder, event.target.value);
    if (placeholder === "Enter your name") {
      setName(event.target.value);
    } else if (placeholder === "Enter your email") {
      setEmail(event.target.value);
    } else if (placeholder === "Enter password") {
      setPassword(event.target.value);
    } else {
      setConfirmPassword(event.target.value);
    }
  };

  const handleSignUp = (event: any) => {
    event.preventDefault();
    if (
      name &&
      email &&
      password &&
      confirmPassword &&
      password === confirmPassword
    ) {
      console.log(name, email, password, confirmPassword);
    }
  };

  return (
    <>
      <div className="login-wrapper h-full flex justify-center items-center">
        <Card>
          <section className="flex">
            <div className="form-field w-96">
              <div className="signup-form">
                <h2 className="form-title mb-6 font-bold text-4xl">Sign up</h2>
                <form
                  onSubmit={handleSignUp}
                  className="register-form"
                  id="register-form"
                >
                  {formList.map((item) => {
                    return (
                      <FormField
                        handleChange={(e: any) =>
                          handleInputChange(e, item.placeholder)
                        }
                        value={item.value}
                        placeholder={item.placeholder}
                        type={item.type}
                      >
                        {item.icon}
                      </FormField>
                    );
                  })}
                  <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                    Register
                  </button>
                </form>
                <div className="mt-8">
                  <h2>
                    Already have an account?{" "}
                    <Link to="/login">
                      <span className="underline text-teal-400">Login</span>
                    </Link>
                  </h2>
                </div>
              </div>
            </div>
            <div className="bg-image flex justify-center items-center">
              <img src={LoginImage} alt="Error loading image" />
            </div>
          </section>
        </Card>
      </div>
    </>
  );
};
export default SignUp;
