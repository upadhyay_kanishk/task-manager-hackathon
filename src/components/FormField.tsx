interface FormFieldType {
  type?: string;
  name?: string;
  placeholder?: string;
  children?: React.ReactNode;
  value?: string;
  handleChange?: (e: any) => void;
}
const defaultProps = {
  placeholder: "Enter Something",
  type: "text",
};

const FormField = (props: FormFieldType) => {
  return (
    <>
      <div className="form-group mb-5 flex items-center">
        <label className="absolute">{props.children}</label>
        <input
          value={props.value}
          type={props.type}
          name={props.name}
          placeholder={props.placeholder}
          onChange={props.handleChange}
        />
      </div>
    </>
  );
};
export default FormField;
FormField.defaultProps = defaultProps;
