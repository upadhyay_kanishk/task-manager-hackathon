import { useState } from "react";
import { PencilFill, PlusCircle, TrashFill } from "react-bootstrap-icons";
import { useParams } from "react-router-dom";
import Modal from "./AddTaskForm";
interface ITableHeader {
  label: string;
}
interface ITableDataSource {
  task: string;
  date: string;
  isCompleted: string;
}

interface TableType {
  headerColumnData: ITableHeader[];
  tableDataSource: ITableDataSource[];
  totalTask: number;
  onDeleteTask: (task: ITableDataSource) => void;
  onUpdateTask: (task: ITableDataSource) => void;
}

const Table = (props: TableType) => {
  const handleDeleteTask = (item: ITableDataSource) => {
    props.onDeleteTask(item);
  };

  const { id } = useParams();
  console.log(id);

  const handleUpdateTask = (item: ITableDataSource) => {
    props.onUpdateTask(item);
  };

  const [showModal, setShowModal] = useState(false);
  const toggleModal = () => setShowModal(!showModal);

  return (
    <div className="static">
      {!props.tableDataSource?.length ? (
        <div role="status">
          <div className="flex justify-center flex-col items-center h-screen">
            <h2>Fetching Data...</h2>
            <svg
              aria-hidden="true"
              className="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600"
              viewBox="0 0 100 101"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                fill="currentColor"
              />
              <path
                d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                fill="currentFill"
              />
            </svg>
          </div>
        </div>
      ) : (
        <div className="overflow-x-auto relative">
          <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr>
                {props.headerColumnData.map((elem, index) => {
                  return (
                    <th key={index} scope="col" className="py-3 px-6">
                      {elem.label}
                    </th>
                  );
                })}
              </tr>
            </thead>
            <tbody>
              {props.tableDataSource.map((elem, index) => {
                console.log(elem);
                return (
                  <tr
                    key={index}
                    className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                  >
                    <th
                      scope="row"
                      className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                    >
                      {elem.task}
                    </th>
                    <td className="py-4 px-6">{elem.date}</td>
                    <td className="py-4 px-6">
                      {elem.isCompleted ? "Yes" : "No"}
                    </td>
                    <td className="py-4 px-6 flex ">
                      <span
                        className="cursor-pointer"
                        onClick={() => handleDeleteTask(elem)}
                      >
                        <TrashFill size={20} />
                      </span>
                      /
                      <span
                        className="cursor-pointer"
                        onClick={() => handleUpdateTask(elem)}
                      >
                        <PencilFill size={20} />
                      </span>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <h2>Total task: {props.totalTask}</h2>
        </div>
      )}
      <Modal showModal={showModal} closeModal={toggleModal} />

      <div className=" cursor-pointer absolute bottom-10 right-10">
        <PlusCircle size={60} onClick={() => toggleModal()} />
      </div>
    </div>
  );
};

Table.defaultProps = {
  headerColumnData: [
    { label: "Task Name" },
    { label: "Created time" },
    { label: "Completed" },
    { label: "Action" },
  ],

};
export default Table;
