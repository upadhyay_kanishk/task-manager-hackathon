export interface CardPropsInterface {
  children: React.ReactNode;
}

const Card = (props: CardPropsInterface) => {
  return (
    <>
      <div className="card-container p-9 bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
        <div className="card-wrapper h-full flex">{props.children}</div>
      </div>
    </>
  );
};

export default Card;
