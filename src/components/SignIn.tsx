import Card from "./Card";
import LoginImage from "../images/signin-image.jpg";
import { Mailbox2, UnlockFill } from "react-bootstrap-icons";
import FormField from "./FormField";
import { FormList } from "../models/form-interface";
import { Link } from "react-router-dom";
import { useState } from "react";

const SignIn = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const formList: FormList[] = [
    {
      value: email,
      placeholder: "Enter Email",
      type: "email",
      icon: <Mailbox2 />,
    },
    {
      value: password,
      placeholder: "Enter Password",
      type: "password",
      icon: <UnlockFill />,
    },
  ];

  const handleInputChange = (event: any, inputType: string | undefined) => {
    inputType === "email"
      ? setEmail(event.target.value)
      : setPassword(event.target.value);
  };

  const handleLogin = (event: any) => {
    event.preventDefault();
    if (email && password) {
      console.log(email, password);
    }
  };

  return (
    <>
      <div className="login-wrapper h-full flex justify-center items-center">
        <Card>
          <section className="flex">
            <div className="form-field w-96">
              <div className="signin-form">
                <h2 className="form-title mb-6 font-bold text-4xl pb-10">
                  Sign In
                </h2>
                <form
                  onSubmit={handleLogin}
                  className="register-form"
                  id="login-form"
                >
                  {formList.map((item) => {
                    return (
                      <FormField
                        handleChange={(e: any) =>
                          handleInputChange(e, item.type)
                        }
                        value={item.value}
                        placeholder={item.placeholder}
                        type={item.type}
                      >
                        {item.icon}
                      </FormField>
                    );
                  })}
                  <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                    Login
                  </button>
                </form>
                <div className="mt-8">
                  <h2>
                    Do'nt have an account?{" "}
                    <Link to="/signup">
                      <span className="underline text-teal-400">
                        Create account
                      </span>
                    </Link>
                  </h2>
                </div>
              </div>
            </div>
            <div className="bg-image flex justify-center items-center">
              <img src={LoginImage} alt="Error loading image" />
            </div>
          </section>
        </Card>
      </div>
    </>
  );
};
export default SignIn;
